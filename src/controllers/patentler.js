const Musteri = require("../models/musteri");
const Patent = require("../models/patent");

exports.getMusteriPatentler = async (req, res) => {
  console.log("getMusteriPatentler");
  Patent.find({musteri: req.params.mid}, (e, patentler) => {
    if (e) {
      return res.status(200).json({error: e, data: null})
    } 
    else {
      return res.status(200).json({error: null, data: patentler})
    }
  })
};

exports.getMusteriPatent = async (req, res) => {
  console.log('getMusteriPatent')
  Patent.findById(req.params.pid, (e, patent) => {
    if (e) {
      return res.status(200).json({error: e, data: null})
    } 
    else {
      return res.status(200).json({error: null, data: patent})
    }
  })
};

exports.addMusteriPatent = async (req, res) => {
  console.log('addMusteriPatent')
  let patent = await new Patent({
    musteri: req.params.mid
    // TODO
  }).save()
  if (patent) {
    return res.status(200).json({error: null, data: patent})
  }
  else {
    return res.status(200).json({error: 'patent can not created', data: null})
  }
};

exports.updateMusteriPatent = async (req, res) => {
  console.log('updateMusteriPatent')
  return res.status(200).json(null)
};

exports.removeMusteriPatent = async (req, res) => {
  console.log('removeMusteriPatent')
  return res.status(200).json(null)
};
