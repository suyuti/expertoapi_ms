const Personel  = require("../models/personel");

exports.getAll = async (req, res) => {
    console.log('get personeller')
    Personel.find({})
    .exec((err, items) => {
        return res.status(200).json(items)
    })
};

exports.getById = async (req, res) => {
    console.log('get personel by Id')
    Personel.findById(req.params.id, (err, item) => {
        return res.status(200).json(item)
    })    
};

exports.create = async (req, res) => {
    console.log('create Personel')
    var payload = req.body
    delete payload._id
    var personel = new Personel(payload)
    personel.createdBy = req.tokenUser
    personel.save((err, item) => {
        if (err) {
            return res.status(200).json(err)
        }
        return res.status(200).json(item)
    })
};

exports.update = async (req, res) => {
};

exports.remove = async (req, res) => {
};

exports.getSatisHedefi = async (req, res) => {
};

exports.getSatisHedefleri = async (req, res) => {
};

exports.createSatisHedefi = async (req, res) => {
};

