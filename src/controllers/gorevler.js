const Gorev = require("../models/gorev");
const ToplantiKarar = require("../models/toplantikarar");
const Yorum = require("../models/yorum");

exports.getAll = async (req, res) => {
  console.log("get gorevler");
  let filters = req.query.filter ? JSON.parse(req.query.filter) : {};
  Gorev.find(filters)
    .populate('musteri', 'marka')
    .lean(true)
    .exec((err, items) => {
        console.log(items)
      return res.status(200).json(items);
    });
};

//--------------------------------------------------------------------------

exports.getById = async (req, res) => {
  console.log("get gorev by Id");
  await Gorev.findById(req.params.id).populate('musteri', 'marka').lean(true).exec((err, item) => {
    return res.status(200).json(item);
  })
  //Gorev.findById(req.params.id, (err, item) => {
  //  return res.status(200).json(item);
  //});
};

//--------------------------------------------------------------------------

exports.create = async (req, res) => {
  console.log("create gorev");
  var payload = req.body;
  delete payload._id;
  var gorev = new Gorev(payload);
  gorev.createdBy = req.tokenUser._id;
  gorev.createdAt = new Date();
  gorev.createdByMsId = req.tokenUser.uniqueId// payload.createdByMsId;
  gorev.sonuc = 'acik'
  //if (!gorev.history) {
  //    gorev.history = []
  gorev.history.push({
    eylem: "Görev oluşturuldu",
    tarih: gorev.createdAt,
    olusturan: gorev.createdBy,
    msid: payload.createdByMsId,
  });
  //}

  gorev.save((err, item) => {
    if (err) {
      return res.status(200).json(err);
    }
    return res.status(200).json(item);
  });
};

//--------------------------------------------------------------------------

exports.update = async (req, res) => {
  console.log("update gorev");
  const id = req.params.id;
  const gelen = req.body
  
  var gorev = await Gorev.findById(id)
  if (!gorev) {
    return res.status(200).json({error: 'Gorev bulunamadi', data: null});
  }
  //if (gelen.atanan.equals(gorev.atanan)) {
    // atanan degistirilmis
  //}

  // toplanti karari gorevi ise  VE kapaniyorsa karari da kapat
  if (gorev.toplantiKarar) {
    if (gelen.sonuc == 'basarili') {
      await ToplantiKarar.findByIdAndUpdate(gorev.toplantiKarar, {$set: {durum: 'Tamamlandı'}})
    }
  }
  
  
  
  var payload = req.body;
  delete payload._id;
  payload.updatedBy = req.tokenUser;
  payload.updatedAt = new Date();

  Gorev.findByIdAndUpdate(id, { $set: payload }, (e, r) => {
    return res.status(200).json(r);
  });
};

//--------------------------------------------------------------------------

exports.remove = async (req, res) => {};

//--------------------------------------------------------------------------

