//const jwt   = require("jsonwebtoken");
const Kisi  = require("../models/kisi");
//const auth  = require("../middlewares/auth");
//const uuid  = require("uuid");

exports.getAll = async (req, res) => {
    console.log('get kisiler')
    Kisi.find({})
    .populate('musteri')
    //.populate('musteriTemsilcisi')
    //.populate('sektor')
    .exec((err, items) => {
        return res.status(200).json(items)
    })
};

exports.getMusteriKisiler = async (req, res) => {
    console.log('get musteri kisiler')
    console.log(req.params.id)
    
    //let filters = req.query.filter || {};
    Kisi.find({musteri: req.params.id})
    //.populate('musteri')
    //.populate('musteriTemsilcisi')
    //.populate('sektor')
    .exec((err, items) => {
        return res.status(200).json(items)
        //return res.status(200).json(['A', 'B'])
    })
};

exports.getById = async (req, res) => {
    console.log('get kisi by Id')
    Kisi.findById(req.params.id).populate('musteri').exec((err, item) => {
        return res.status(200).json(item)
    })
};

exports.create = async (req, res) => {
    console.log('create kisi')
    var payload = req.body
    let kisi = await new Kisi(payload).save()
    return res.status(200).json(kisi)
};

exports.update = async (req, res) => {
    console.log('kisi update')
    const payload = req.body
    const mid = req.params.id

    console.log(payload)

    Kisi.findByIdAndUpdate(mid, 
        payload,
        //{$set:payload}, 
        {new:true, lean:true, upsert: false, strict: true}, (e, r) => {
        //console.log(r)
        return res.status(200).json(r)
    })
};

exports.remove = async (req, res) => {
};
