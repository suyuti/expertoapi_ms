//const jwt   = require("jsonwebtoken");
const Sektor  = require("../models/sektor");
//const auth  = require("../middlewares/auth");
//const uuid  = require("uuid");

exports.getAll = async (req, res) => {
    console.log('get sektorler')
    Sektor.find({})
    .exec((err, items) => {
        return res.status(200).json(items)
    })
};

exports.getById = async (req, res) => {
    console.log('get sektor by Id')
    Sektor.findById(req.params.id, (err, item) => {
        return res.status(200).json(item)
    })    
};

exports.create = async (req, res) => {
    console.log('create Sektor')
    var payload = req.body
    delete payload._id
    var sektor = new Sektor(payload)
    sektor.createdBy = req.tokenUser
    sektor.save((err, item) => {
        if (err) {
            return res.status(200).json(err)
        }
        return res.status(200).json(item)
    })
};

exports.update = async (req, res) => {
};

exports.remove = async (req, res) => {
};
