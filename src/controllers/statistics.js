const Gorev = require("../models/gorev");
const Toplanti = require("../models/toplanti");

exports.getGorevStats = async (req, res) => {
  console.log("get gorev stats");
  let olusturduklarim = await Gorev.countDocuments({createdBy: req.token})
  let banaAtanmisAcik = await Gorev.countDocuments({sonuc: 'acik'})
  return res.status(200).json(
    {
      olusturduklarim: olusturduklarim,
      acik: banaAtanmisAcik
    });
};

exports.getToplantiStats = async (req, res) => {
  console.log("get toplanti stats");
  let olusturduklarim = await Toplanti.countDocuments({createdBy: req.token})
  let banaAtanmisAcik = await Toplanti.countDocuments({expertoKatilimcilar: 'acik'})
  return res.status(200).json(
    {
      olusturduklarim: olusturduklarim,
      acik: banaAtanmisAcik
    });
};

