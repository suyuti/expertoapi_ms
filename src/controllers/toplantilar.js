const Toplanti = require("../models/toplanti");
const ToplantiKarar = require("../models/toplantikarar");
const pdf = require("pdfjs");
const fs = require("fs");
const Pdf = require('../utils/toplantiPdf')

exports.getAll = async (req, res) => {
  console.log("get toplantilar");
  let filters = req.query.filter ? JSON.parse(req.query.filter) : {};
  Toplanti.find(filters)
    .populate("firma", "marka")
    .exec((err, items) => {
      return res.status(200).json(items);
    });
};

//--------------------------------------------------------------------------

exports.getById = async (req, res) => {
  console.log("get toplanti by Id");

  Toplanti.findById(req.params.id)
    .populate("firma")
    .lean(true)
    .exec((err, item) => {
      if (err) {
        return res.status(200).jsno({ err: err, data: null });
      }
      return res.status(200).json({
        data: {
          ...item,
          kararlar: [
            {
              karar: "karar",
              expertoAtama: "123",
              sonTarih: "22.07.2020",
              durum: "acik",
            },
          ],
        },
        err: null,
      });
    });
};

//--------------------------------------------------------------------------

exports.create = async (req, res) => {
  console.log("create toplanti");
  var payload = req.body;
  delete payload._id;
  var toplanti = new Toplanti(payload);
  toplanti.createdBy = req.tokenUser._id;
  toplanti.createdAt = new Date();
  toplanti.createdByMsId = payload.createdByMsId;

  //if (!toplanti.history) {
  //    toplanti.history = []
  toplanti.history.push({
    eylem: "create",
    tarih: toplanti.createdAt,
    olusturan: toplanti.createdBy,
    msid: payload.createdByMsId,
  });
  //}

  toplanti.save((err, item) => {
    if (err) {
      return res.status(200).json(err);
    }
    return res.status(200).json(item);
  });
};

//--------------------------------------------------------------------------

exports.update = async (req, res) => {
  console.log("update toplanti");
  const id = req.params.id;
  var payload = req.body;
  delete payload._id;
  payload.updatedBy = req.tokenUser;
  payload.updatedAt = new Date();
  Toplanti.findByIdAndUpdate(id, { $set: payload }, (e, r) => {
    console.log(e);
    return res.status(200).json(r);
  });
};

//--------------------------------------------------------------------------

exports.remove = async (req, res) => {};

//--------------------------------------------------------------------------

exports.kararEkle = async (req, res) => {};

exports.kararGuncelle = async (req, res) => {};

exports.getPdf = async (req, res) => {
  console.log('toplanti pdf')
  const id = req.params.id
  let toplanti = await Toplanti.findById(id).populate('firma', 'marka').lean(true)
  let kararlar = await ToplantiKarar.find({toplantiId: id}).lean(true)
  toplanti.kararlar=[...kararlar]
  Pdf.createToplantiPdf(toplanti).then((b) => {
    res.end(b);
  });
};
