const Musteri = require("../models/musteri");
const MusteriUrun = require("../models/musteri_urunler");

exports.getMusteriUrunler = async (req, res) => {
  console.log("getMusteriUrunler");
  MusteriUrun.find({musteri: req.params.mid}).populate('urun').exec((e, urunler) => {
    if (e) {
      return res.status(200).json({error: e, data: null})
    } 
    else {
      return res.status(200).json({error: null, data: urunler})
    }
  })
};

exports.addMusteriUrun = async (req, res) => {
  console.log('addMusteriUrun')
  let musteriUrun = await new MusteriUrun({
    musteri: req.params.mid,
    urun: req.body.urun
    // TODO
  }).save()
  if (patent) {
    return res.status(200).json({error: null, data: musteriUrun})
  }
  else {
    return res.status(200).json({error: 'musteriurun can not created', data: null})
  }
};

