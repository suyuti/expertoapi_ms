const Yorum = require("../models/yorum");
const Gorev = require("../models/gorev");

exports.getYorumlar = async (req, res) => {
  console.log('get yorumlar')
  const id = req.params.refId
  let yorumlar = await Yorum.find({referans: id})
  return res.status(200).json(yorumlar)
};

exports.addYorum = async (req, res) => {
  console.log('add yorum')
  const id = req.params.refId
  let yorum = await new Yorum({
    referans: id,
    yorum: req.body.yorum,
    olusturanMsID: req.body.olusturanMsID,
    olusturan: req.tokenUser,
    olusturmaTarihi: new Date()
  }).save()

  var gorev = await Gorev.findById(id)
  gorev.history.push({
    eylem: 'Yorum eklendi',
    tarih: yorum.olusturmaTarihi,
    olusturan: yorum.olusturan,
    msid: yorum.olusturanMsID 
  })
  await gorev.save()

  return res.status(200).json(yorum)
};
