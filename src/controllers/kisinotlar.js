const KisiNot = require("../models/kisi_not");

exports.getAll = async (req, res) => {
  console.log("get kisi notlar");
  KisiNot.find({ kisi: req.params.kid }).exec((err, items) => {
    if (err) {
      return res.status(200).json({ error: err, data: null });
    }
    return res.status(200).json({ error: null, data: items });
  });
};

exports.create = async (req, res) => {
  console.log("create kisi not");
  var payload = req.body;
  let not = await new KisiNot({
      kisi: req.params.kid,
      not: req.body.not,
      tarih: new Date()
  }).save();
  if (!not) {
    return res.status(200).json({ error: "Not olusturulamadi", data: null });
  } else {
    return res.status(200).json({ error: null, data: not });
  }
};
