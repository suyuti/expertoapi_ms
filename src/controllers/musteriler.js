//const jwt   = require("jsonwebtoken");
const Musteri  = require("../models/musteri");
//const auth  = require("../middlewares/auth");
//const uuid  = require("uuid");

exports.getAll = async (req, res) => {
    console.log('get musteriler')
    let filters = req.query.filter ? JSON.parse(req.query.filter) : {};
    //let filters = req.query.filter || {};
    Musteri.find(filters)
    //.populate('musteriTemsilcisi')
    .populate('sektor')
    .exec((err, items) => {
        return res.status(200).json(items)
    })
};

exports.getById = async (req, res) => {
    console.log('get musteri by Id')
    Musteri.findById(req.params.id).populate('sektor').exec((err, item) => {
        return res.status(200).json(item)
    })


    //Musteri.findById(req.params.id, (err, item) => {
    //    return res.status(200).json(item)
    //})    
};

exports.create = async (req, res) => {
    console.log('create musteri')
    var payload = req.body
    delete payload._id
    var musteri = new Musteri(payload)
    musteri.createdBy = req.tokenUser
    musteri.save((err, item) => {
        if (err) {
            return res.status(200).json(err)
        }
        return res.status(200).json(item)
    })
};

exports.update = async (req, res) => {
    console.log('Musteri update')
    const payload = req.body
    const mid = req.params.id

    console.log(payload)

    Musteri.findByIdAndUpdate(mid, 
        payload,
        //{$set:payload}, 
        {new:true, lean:true, upsert: false, strict: true}, (e, r) => {
        //console.log(r)
        return res.status(200).json(r)
    })

};

exports.remove = async (req, res) => {
};
