const jwt = require("jsonwebtoken");
const Personel = require("../models/personel");
const Yetki = require("../models/yetki");

exports.signin = async (req, res, next) => {
  console.log("sign in");
  const token = req.headers.authorization; //.replace("Bearer ", "").trim();
  const uniqueId = req.query.uniqueId;

  let personel = await Personel.findOne({ uniqueId: uniqueId });
  if (personel) {
    let yetkiler = await Yetki.find({ rol: personel.rol }).lean(true);
    var yetki = [];
    if (yetkiler.length > 0) {
      yetki = yetkiler[0].yetkiler;
    }
    const expertoToken = jwt.sign(
      {
        //token: token,
        uniqueId: uniqueId,
      },
      "SELAM"
    );
    return res
      .status(200)
      .json({
        expertoToken: expertoToken,
        redirectUrl: personel.homePage,
        yetkiler: yetki,
      });
  }
  return res.status(401).send("user not found");
};

//-------------------------------------------------------------------------------------------------
