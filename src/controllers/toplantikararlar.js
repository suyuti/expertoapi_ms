const Toplanti = require("../models/toplanti");
const Gorev = require("../models/gorev");
const ToplantiKararlar = require("../models/toplantikarar");
const moment = require("moment");

exports.getAll = async (req, res) => {
  console.log("get toplanti kararlari");
  console.log(req.params.id);
  let filters = req.query.filter ? JSON.parse(req.query.filter) : {};
  let toplanti = await Toplanti.findById(req.params.id).lean(true);

  if (!toplanti) {
    return res.status(200).json({ error: "toplanti bulunamadi", data: null });
  }

  ToplantiKararlar.find({ toplantiId: req.params.id }, (e, items) => {
    if (e) {
      return res.status(200).json({ error: e, data: null });
    }
    return res.status(200).json({ error: null, data: items });
  });
};

//--------------------------------------------------------------------------

exports.getById = async (req, res) => {
  console.log("get toplanti by Id");

  Toplanti.findById(req.params.id)
    .populate("firma")
    .lean(true)
    .exec((err, item) => {
      if (err) {
        return res.status(200).jsno({ err: err, data: null });
      }
      return res.status(200).json({
        data: {
          ...item,
          kararlar: [
            {
              karar: "karar",
              expertoAtama: "123",
              sonTarih: "22.07.2020",
              durum: "acik",
            },
          ],
        },
        err: null,
      });
    });
};

//--------------------------------------------------------------------------

exports.create = async (req, res) => {
  console.log("create toplanti");
  var payload = req.body;
  delete payload._id;
  var toplanti = new Toplanti(payload);
  toplanti.createdBy = req.tokenUser._id;
  toplanti.createdAt = new Date();
  toplanti.createdByMsId = payload.createdByMsId;

  //if (!toplanti.history) {
  //    toplanti.history = []
  toplanti.history.push({
    eylem: "create",
    tarih: toplanti.createdAt,
    olusturan: toplanti.createdBy,
    msid: payload.createdByMsId,
  });
  //}






  toplanti.save((err, item) => {
    if (err) {
      return res.status(200).json(err);
    }
    return res.status(200).json(item);
  });
};

//--------------------------------------------------------------------------

exports.update = async (req, res) => {
  console.log("update toplanti");
  const id = req.params.id;
  var payload = req.body;
  delete payload._id;
  payload.updatedBy = req.tokenUser;
  payload.updatedAt = new Date();
  Toplanti.findByIdAndUpdate(id, { $set: payload }, (e, r) => {
    console.log(e);
    return res.status(200).json(r);
  });
};

//--------------------------------------------------------------------------

exports.remove = async (req, res) => {};

//--------------------------------------------------------------------------

exports.kararEkle = async (req, res) => {};

exports.kararGuncelle = async (req, res) => {};

exports.saveKararlar = async (req, res) => {
  try {
    console.log("save toplanti kararlari");
    const kararlar = req.body;

    let toplanti = await Toplanti.findById(req.params.id).lean(true);

    if (!toplanti) {
      return res.status(200).json({ error: "toplanti bulunamadi", data: null });
    }
    for (var i = 0; i < kararlar.length - 1; ++i) {
      var karar = kararlar[i];

      if (karar._id) {
        // update
        let _karar = { ...karar };
        delete _karar._id;
        let r = await ToplantiKararlar.findByIdAndUpdate(
          karar._id,
          { $set: _karar },
          { new: true, lean: true, upsert: false, strict: true }
        );
      } else {
        // create
        delete karar._id
        var __karar = new ToplantiKararlar(karar);
        __karar.toplantiId = req.params.id;
        let _karar = await __karar.save()


        var durum =''
        var gorevOlustur = false
        if (_karar.durum === 'Açık') {
          durum='acik'
          gorevOlustur = true
        }
        else if (_karar.durum === 'Tamamlandı') {
          durum='basarili'
          gorevOlustur = true
        }

        if (gorevOlustur) {
          console.log('Toplanti kararindan gorev olusturulur')
          console.log(`Toplanti karar : ${_karar.karar} [${_karar._id}]`)
          let gorev = await new Gorev({
            baslik        : _karar.karar,
            aciklama      : 'Toplantı kararından otomatik üretilmiştir',
            atananMsId    : _karar.expertoAtamaMsId,
            sonuc         : durum,
            baslamaTarihi : toplanti.tarih, // toplanti tarihi olacak
            sonTarih      : moment(_karar.sonTarih, 'DD.mm.yyyy').format('mm.DD.yyyy'),
            musteri       : toplanti.firma,
            createdByMsId : req.tokenUser.uniqueId,
            toplantiKarar : _karar._id
          }).save()


        }


      }
    }
    return res.status(200).json({});
  } catch (e) {
    console.log(e);
  }
};

String.prototype.isEmpty = function () {
  return this.length === 0 || !this.trim();
};
