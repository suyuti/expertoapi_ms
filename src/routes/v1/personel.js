const express = require('express')
const router = express.Router()
const trimRequest = require('trim-request')

const PersonelController = require('../../controllers/personeller')

router.get('/',                     trimRequest.all, PersonelController.getAll)
router.get('/:id',                  trimRequest.all, PersonelController.getById)
router.get('/:id/satishedefi',      trimRequest.all, PersonelController.getSatisHedefi)
router.get('/:id/satishedefleri',   trimRequest.all, PersonelController.getSatisHedefleri)

router.put('/:id/satishedefi',      trimRequest.all, PersonelController.createSatisHedefi)

module.exports = router
