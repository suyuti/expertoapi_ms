const express = require('express')
const router = express.Router()
const trimRequest = require('trim-request')

const UrunController = require('../../controllers/urunler')

router.get('/',           trimRequest.all, UrunController.getAll)
router.get('/:id',        trimRequest.all, UrunController.getById)


module.exports = router
