const express = require('express')
const router = express.Router()
const trimRequest = require('trim-request')

const MusteriController = require('../../controllers/musteriler')
const KisiController = require('../../controllers/kisiler')
const PatentController = require('../../controllers/patentler')
const MusteriUrunController = require('../../controllers/musteri_urun')

router.get('/',                 trimRequest.all, MusteriController.getAll)
router.get('/:id',              trimRequest.all, MusteriController.getById)
router.get('/:id/kisiler',      trimRequest.all, KisiController.getMusteriKisiler)
router.patch('/:id',            trimRequest.all, MusteriController.update)

// Patentler
router.get('/:mid/patent',          trimRequest.all, PatentController.getMusteriPatentler)
router.get('/:mid/patent/:pid',     trimRequest.all, PatentController.getMusteriPatent)
router.post('/:mid/patent',         trimRequest.all, PatentController.addMusteriPatent)
router.patch('/:mid/patent/:pid',   trimRequest.all, PatentController.updateMusteriPatent)
router.delete('/:mid/patent/:pid',  trimRequest.all, PatentController.removeMusteriPatent)

// Urunleri
router.get('/:mid/urun',          trimRequest.all, MusteriUrunController.getMusteriUrunler)
router.post('/:mid/urun',         trimRequest.all, MusteriUrunController.addMusteriUrun)


module.exports = router
