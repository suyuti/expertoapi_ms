const routes = require('express').Router();

const auth              = require('./auth'          );
const musteri           = require('./musteri'       );
const urun              = require('./urun'          );
const kisi              = require('./kisi'          );
const sektor            = require('./sektor'        );
const personel          = require('./personel'      );
const gorev             = require('./gorev'         );
const toplanti          = require('./toplanti'      );
const statistics        = require('./statistics'    );
const yorum             = require('./yorum'         );

routes.use('/v1/auth',          auth            );
routes.use('/v1/musteri',       musteri         );
routes.use('/v1/urun',          urun            );
routes.use('/v1/kisi',          kisi            );
routes.use('/v1/sektor',        sektor          );
routes.use('/v1/personel',      personel        );
routes.use('/v1/gorev',         gorev           );
routes.use('/v1/toplanti',      toplanti        );
routes.use('/v1/statistics',    statistics      );
routes.use('/v1/yorum',         yorum           );

module.exports = routes;
