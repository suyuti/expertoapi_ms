const express = require('express')
const router = express.Router()
const trimRequest = require('trim-request')

const SektorController = require('../../controllers/sektorler')

router.get('/',           trimRequest.all, SektorController.getAll)


module.exports = router
