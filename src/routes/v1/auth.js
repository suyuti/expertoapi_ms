const express = require('express')
const router = express.Router()
const trimRequest = require('trim-request')

const AuthController = require('../../controllers/auth')

router.get('/signin',           trimRequest.all, AuthController.signin)
router.get('/signout',          trimRequest.all, AuthController.signin)
router.post('/callback',        trimRequest.all, AuthController.signin)


module.exports = router
