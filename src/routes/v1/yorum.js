const express = require('express')
const router = express.Router()
const trimRequest = require('trim-request')

const YorumController = require('../../controllers/yorumlar')

router.get('/:refId',          trimRequest.all, YorumController.getYorumlar)
router.post('/:refId',         trimRequest.all, YorumController.addYorum)


module.exports = router
