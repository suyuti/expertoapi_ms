const express = require('express')
const router = express.Router()
const trimRequest = require('trim-request')

const ToplantiController        = require('../../controllers/toplantilar')
const ToplantiKararController   = require('../../controllers/toplantikararlar')

router.get('/',                     trimRequest.all, ToplantiController.getAll      )
router.get('/:id',                  trimRequest.all, ToplantiController.getById     )
router.post('/',                    trimRequest.all, ToplantiController.create      )
router.patch('/:id',                trimRequest.all, ToplantiController.update      )
router.delete('/:id',               trimRequest.all, ToplantiController.remove      )
router.get('/:id/pdf',              trimRequest.all, ToplantiController.getPdf      )

router.get('/:id/karar',            trimRequest.all, ToplantiKararController.getAll )
router.get('/:id/karar/:kararId',   trimRequest.all, ToplantiKararController.getById)
router.post('/:id/karar',           trimRequest.all, ToplantiKararController.create )
router.patch('/:id/karar/:kid',     trimRequest.all, ToplantiKararController.update )
router.delete('/:id/karar/:kid',    trimRequest.all, ToplantiKararController.remove         )

router.post('/:id/kararlar',        trimRequest.all, ToplantiKararController.saveKararlar   )

module.exports = router
