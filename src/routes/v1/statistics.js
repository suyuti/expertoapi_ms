const express = require('express')
const router = express.Router()
const trimRequest = require('trim-request')

const StatisticsController = require('../../controllers/statistics')

router.get('/gorev',           trimRequest.all, StatisticsController.getGorevStats)
router.get('/toplanti',        trimRequest.all, StatisticsController.getToplantiStats)


module.exports = router
