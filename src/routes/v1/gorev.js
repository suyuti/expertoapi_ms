const express = require('express')
const router = express.Router()
const trimRequest = require('trim-request')

const GorevController = require('../../controllers/gorevler')

router.get('/',             trimRequest.all, GorevController.getAll)
router.get('/:id',          trimRequest.all, GorevController.getById)
router.post('/',            trimRequest.all, GorevController.create)
router.patch('/:id',        trimRequest.all, GorevController.update)
router.delete('/:id',       trimRequest.all, GorevController.remove)



module.exports = router
