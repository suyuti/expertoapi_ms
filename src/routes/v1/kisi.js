const express = require('express')
const router = express.Router()
const trimRequest = require('trim-request')

const KisiController = require('../../controllers/kisiler')
const KisiNotController = require('../../controllers/kisinotlar')

router.get('/',             trimRequest.all, KisiController.getAll)
router.get('/:id',          trimRequest.all, KisiController.getById)
router.post('/',            trimRequest.all, KisiController.create)
router.patch('/:id',        trimRequest.all, KisiController.update)

router.get('/:kid/not',      trimRequest.all, KisiNotController.getAll)
router.post('/:kid/not',     trimRequest.all, KisiNotController.create)


module.exports = router
