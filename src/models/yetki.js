const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var SchemaModel = new Schema(
  {
    rol        : { type: String, required: true  },
    yetkiler    : Object
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

module.exports = mongoose.models.Yetki || mongoose.model("Yetki", SchemaModel);
