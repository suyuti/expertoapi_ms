const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var SchemaModel = new Schema(
  {
    musteri         : { type: mongoose.Schema.Types.ObjectId, ref: 'Musteri' },
    urun            : { type: mongoose.Schema.Types.ObjectId, ref: 'Urun' },
    baslangicTarihi : { type: Date },
    bitisTarihi     : { type: Date },
    aktif           : { type: Boolean, default: false}
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

module.exports =
  mongoose.models.MusteriUrunler || mongoose.model("MusteriUrunler", SchemaModel);
