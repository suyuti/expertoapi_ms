const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var SchemaModel = new Schema(
  {
    //role: { type: String, enum: roles, default: "zi" },
    uniqueId: { type: String },
    homePage: { type: String },
    rol: { type: String, default: "newUser" },
    digerPersoneller: [String], // Islem yapabilecegi diger personeller MsId
    satis: {
      satisHedefleri: [
        {
          ay: { type: String },
          hedefCiro: { type: Number },
          hedefToplanti: { type: Number },
        },
      ],
    },
    indirimOrani: { type: Number, default: 0 },
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

module.exports =
  mongoose.models.Personel || mongoose.model("Personel", SchemaModel);
