const mongoose = require("mongoose");

const Schema = mongoose.Schema;

var SchemaModel = new Schema(
  {
    toplantimsid  : String,
    baslik        : { type: String, required: true },
    aciklama      : String,
    tarih         : Date,
    saat          : String, 
    yer           : String,
    sure          : String,
    
    tur: {type: String, enum: ['yuzyuze', 'telekonferans', 'videokonferans']},
    firma: { type: mongoose.Schema.Types.ObjectId, ref: "Musteri" },
    expertoKatilimcilar: [String],
    firmaKatilimcilar: [String],
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: "Personel" },
    createdByMsId: String, // olusturan kisinin MSID
    createdAt: Date,
    updatedBy: { type: mongoose.Schema.Types.ObjectId, ref: "Personel" },
    updatedAt: Date,
    sonuc: String,
    kararlar: [{ type: mongoose.Schema.Types.ObjectId, ref: "ToplantiKarar" }],
    history: [
      {
        eylem: String,
        tarih: Date,
        olusturan: { type: mongoose.Schema.Types.ObjectId, ref: "Personel" },
        msid: String,
      },
    ],
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

SchemaModel.pre("save", function (next) {
  return next();
});

module.exports =
  mongoose.models.Toplanti || mongoose.model("Toplanti", SchemaModel);
