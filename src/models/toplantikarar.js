const mongoose = require("mongoose");

const Schema = mongoose.Schema;

var SchemaModel = new Schema(
  {
    toplantiId              : { type: mongoose.Schema.Types.ObjectId, ref: "Toplanti" },
    karar                   : String,
    expertoAtamaMsId        : String,
    expertoAtama            : String,
    expertoAtamaDisplayName : String,
    sonTarih                : String,
    durum                   : String,
    ilgiliGorev             : { type: mongoose.Schema.Types.ObjectId, ref: "Gorev" },
    createdBy               : { type: mongoose.Schema.Types.ObjectId, ref: "Personel" },
    createdByMsId           : String, // olusturan kisinin MSID
    createdAt               : Date,
    updatedBy               : { type: mongoose.Schema.Types.ObjectId, ref: "Personel" },
    updatedAt               : Date,
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

SchemaModel.pre("save", function (next) {
  return next();
});

module.exports =
  mongoose.models.ToplantiKarar || mongoose.model("ToplantiKarar", SchemaModel);
