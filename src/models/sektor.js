const mongoose = require("mongoose");

const Schema = mongoose.Schema;

var SchemaModel = new Schema(
  {
      adi       : { type: String, required: true  },
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

SchemaModel.pre("save", function (next) {
    return next();
});

module.exports = mongoose.models.Sektor || mongoose.model("Sektor", SchemaModel);
