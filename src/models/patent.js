const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var SchemaModel = new Schema(
  {
    musteri       : { type: String },
    patentNo      : { type: String },
    baslik        : { type: String },
    Konu          : { type: String },
    basvuruTarihi : { type: String },
    tescilTarihi  : { type: String },
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

module.exports =
  mongoose.models.Patent || mongoose.model("Patent", SchemaModel);
