const mongoose = require("mongoose");

const Schema = mongoose.Schema;

var SchemaModel = new Schema(
  {
    referans          : { type: String }, 
    yorum             : { type: String }, 
    olusturanMsID     : String,
    olusturan         : { type: mongoose.Schema.Types.ObjectId, ref: 'Personel'  },
    olusturmaTarihi   : { type: Date  },
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

SchemaModel.pre("save", function (next) {
    return next();
});

module.exports = mongoose.models.Yorum || mongoose.model("Yorum", SchemaModel);
