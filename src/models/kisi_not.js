const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var SchemaModel = new Schema(
  {
    kisi        : { type: mongoose.Schema.Types.ObjectId, ref: 'Kisi' },
    not         : { type: String },
    writer      : { type: mongoose.Schema.Types.ObjectId, ref: 'Personel' },
    writerMsId  : { type: String },
    tarih       : { type: Date },
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

module.exports =
  mongoose.models.KisiNot || mongoose.model("KisiNot", SchemaModel);
