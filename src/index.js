//https://stackoverflow.com/questions/21397809/create-a-trusted-self-signed-ssl-cert-for-localhost-for-use-with-express-node

require("dotenv").config();
var express = require('express')
var fs = require('fs')
var https = require('https')
const bodyParser = require("body-parser");
const cors = require("cors");
const initMongo = require("./config/mongo");
const token = require("./middlewares/token");
var passport = require("passport");
var OIDCStrategy = require("passport-azure-ad").OIDCStrategy;

var users = {};

passport.serializeUser(function (user, done) {
  // Use the OID property of the user as a key
  users[user.profile.oid] = user;
  done(null, user.profile.oid);
});

passport.deserializeUser(function (id, done) {
  done(null, users[id]);
});

const oauth2 = require("simple-oauth2").create({
  client: {
    id: process.env.OAUTH_APP_ID,
    secret: process.env.OAUTH_APP_PASSWORD,
  },
  auth: {
    tokenHost: process.env.OAUTH_AUTHORITY,
    authorizePath: process.env.OAUTH_AUTHORIZE_ENDPOINT,
    tokenPath: process.env.OAUTH_TOKEN_ENDPOINT,
  },
});

async function signInComplete(
  iss,
  sub,
  profile,
  accessToken,
  refreshToken,
  params,
  done
) {
  if (!profile.oid) {
    return done(new Error("No OID found in user profile."));
  }

  try {
    const user = await graph.getUserDetails(accessToken);

    if (user) {
      // Add properties to profile
      profile["email"] = user.mail ? user.mail : user.userPrincipalName;
    }
  } catch (err) {
    return done(err);
  }

  // Create a simple-oauth2 token from raw tokens
  let oauthToken = oauth2.accessToken.create(params);

  // Save the profile and tokens in user storage
  users[profile.oid] = { profile, oauthToken };
  return done(null, users[profile.oid]);
}

passport.use(
  new OIDCStrategy(
    {
      identityMetadata: `${process.env.OAUTH_AUTHORITY}${process.env.OAUTH_ID_METADATA}`,
      clientID: process.env.OAUTH_APP_ID,
      responseType: "code id_token",
      responseMode: "form_post",
      redirectUrl: process.env.OAUTH_REDIRECT_URI,
      allowHttpForRedirectUrl: true,
      clientSecret: process.env.OAUTH_APP_PASSWORD,
      validateIssuer: false,
      passReqToCallback: false,
      scope: process.env.OAUTH_SCOPES.split(" "),
    },
    signInComplete
  )
);


var app = express()
const port = process.env.PORT || 4000
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: "200mb" }));
app.use(cors());
app.use(token.TokenControl);
app.use(require("./routes/v1"));
initMongo();

https.createServer({
  //key : fs.readFileSync(__dirname + '/../certs/selfsigned.key'),
  //cert : fs.readFileSync(__dirname + '/../certs/selfsigned.crt')
  key : fs.readFileSync(__dirname +  '/../security/ca.key', 'utf8'),
  cert : fs.readFileSync(__dirname + '/../security/ca.cer', 'utf8')
}, app)
.listen(port, function () {
  console.log(`https listening on ${port}`)
})