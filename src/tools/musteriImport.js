const initMongo = require("../config/mongo");
const Excel     = require('exceljs')
const Musteri   = require('../models/musteri')
const Sektor    = require('../models/sektor')
const Kisi      = require('../models/kisi')


const processMusteriExcel = async () => {
    const workbook = new Excel.Workbook();
    await workbook.xlsx.readFile('./data/musteri.xlsx');

    // select first sheet
    const sheet = workbook.getWorksheet(1)

    await Musteri.deleteMany()
    await Kisi.deleteMany()
    await Sektor.deleteMany()

    /* 
        A - MARKA	
        B - FİRMA ÜNVANI	
        C - SEKTÖR	
        D - FİRMA ADRESİ	
        E - İL	
        F - İLÇE	
        G - FABRİKA ADRESİ	
        H - İL 	
        I - İLÇE	
        J - VERGİ DAİRESİ	
        K - VERGİ NUMARASI	
        L - FİRMA TELEFONU	
        M - WEB SİTESİ	
        N - DOMAİN	
        O - KİŞİ 1 ADI	
        P - KİŞİ 1 SOYADI	
        Q - KİŞİ 1 ÜNVANI	
        R - KİŞİ 1 CİNSİYETİ	
        S - KİŞİ 1 DEPARTMANI	
        T - KİŞİ 1 CEP TELEFONU	
        U - KİŞİ 1 SABİT TELEFONU	
        V - KİŞİ 1 DAHİLİ	
        W - KİŞİ 1 MAİL	
        ...
        14
    */
    for (var i = 1; i < sheet.rowCount; ++i) {
        const row = sheet.getRow(i+1)

        let marka               = row.getCell('A').text.trim()
        let firmaUnvani         = row.getCell('B').text.trim()
        let sektorAdi           = row.getCell('C').text.trim()
        let firmaAdresi         = row.getCell('D').text.trim()
        let il                  = row.getCell('E').text.trim()
        let ilce                = row.getCell('F').text.trim()
        
        let fabrikaAdresi       = row.getCell('G').text.trim()
        let fabrikaAdresiIl     = row.getCell('H').text.trim()
        let fabrikaAdresiIlce   = row.getCell('I').text.trim()

        let vd                  = row.getCell('J').text.trim()
        let vn                  = row.getCell('K').text.trim()
        let firmaTelefon        = row.getCell('L').text
        let web                 = '' //row.getCell('M').text.trim()
        let domain              = '' //row.getCell('N').text.trim()

        if (!marka           &&
            !firmaUnvani     &&
            !sektorAdi       &&
            !firmaAdresi     &&
            !il              &&
            !ilce            &&
            !vd              &&
            !vn              &&
            !firmaTelefon    &&
            !web             &&
            !domain          ) {
                break;
            }


        if (sektorAdi) {
            var sektor = await Sektor.findOne({adi: sektorAdi})
            if (!sektor) {
                console.log(marka)
                sektor = await new Sektor({adi: sektorAdi}).save()
            }
        }
        // 1. Firma unvan yok ise durumu: Potansiyel olur
        var durum;
        if (!firmaUnvani) {
            durum= 'potansiyel'
        }
        else {
            durum = 'aktif'
        }


        let musteri = await new Musteri({
            marka   : marka || firmaUnvani,
            unvan   : firmaUnvani,
            domain  : domain,
            sektor  : sektor,
            durum   : durum,

            iletisim: {
                adres: firmaAdresi,
                fabrikaAdresi: fabrikaAdresi,
                il: il,
                ilce: ilce,
                telefon: firmaTelefon,
                webSitesi: web
            },

            firmaBilgi: {
                vergiDairesi : vd,
                vergiNo: vn
            }

        }).save()
        
        for (var k = 0; k < 15; ++k) {
            let kisiAdi     = row.getCell((k * 9) + 0 + 15).text.trim()
            let kisiSoyadi  = row.getCell((k * 9) + 1 + 15).text.trim()
            let unvani      = row.getCell((k * 9) + 2 + 15).text.trim()
            let cinsiyet    = row.getCell((k * 9) + 3 + 15).text.trim()
            let departman   = row.getCell((k * 9) + 4 + 15).text.trim()
            let cepTel      = row.getCell((k * 9) + 5 + 15).text.trim()
            let sabitTel    = row.getCell((k * 9) + 6 + 15).text.trim()
            let dahili      = row.getCell((k * 9) + 7 + 15).text.trim()
            let mail        = row.getCell((k * 9) + 8 + 15).text.trim()

            console.log(kisiAdi)

            if (!kisiAdi     &&
                !kisiSoyadi  &&
                !unvani      &&
                !cinsiyet    &&
                !departman   &&
                !cepTel      &&
                !sabitTel    &&
                !dahili      &&
                !mail        ) {
                    continue
                }

            let kisi = await new Kisi({
                musteri     : musteri,
                adi         : kisiAdi,
                soyadi      : kisiSoyadi,
                unvani      : unvani,
                cinsiyet    : cinsiyet,
                departman   : departman,
                cepTelefon  : cepTel,
                isTelefon   : sabitTel,
                dahili      : dahili,
                mail        : mail
            }).save()

            
            
            //console.log(kisiAdi)
        }
        
        
        
        
        //console.log(`${i} ${marka}`)
    }
}

initMongo();
processMusteriExcel().then(() => {
    console.log('bitti')
})
