var express = require("express");
require("dotenv").config();
const bodyParser = require("body-parser");
const cors = require("cors");
const initMongo = require("./config/mongo");
const token = require("./middlewares/token");

var app = express();
app.set("port", process.env.PORT || 4000);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: "200mb" }));
app.use(cors());
app.use(token.TokenControl);
app.use(require("./routes/v1"));
initMongo();
app.listen(app.get("port"));

