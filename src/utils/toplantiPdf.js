const pdf = require("pdfjs");
const fs = require("fs");
const moment = require("moment");

const createToplantiPdf = async (toplanti) => {
  const font = new pdf.Font(fs.readFileSync('./fonts/open-sans/OpenSans-Regular.ttf'))

  const src = fs.readFileSync("./experto-logo.jpg");
  const logo = new pdf.Image(src);
  var doc = new pdf.Document({
    font: font,
  });

  var header = doc
    .header()
    .table({ widths: [null, null, null], paddingBottom: 1 * pdf.cm })
    .row();
  header.cell().image(logo, { height: 1 * pdf.cm });
  header.cell().text().add(new Date().toLocaleDateString())
  header.cell()
    .text({ textAlign: "right" })
    .add("Adres")
    .add("experto", {
    link: "https://experto.com.tr",
    underline: false,
    color: 0x569cd6,
  });

  doc.footer().pageNumber(
    function (curr, total) {
      return curr + " / " + total;
    },
    { textAlign: "center" }
  );

  var cell = doc.cell({ paddingBottom: 0.5 * pdf.cm });
  cell.text("Toplantı Notları", {
    fontSize: 22,
    font: font
  });
  cell.text(`Firma ${toplanti.firma.marka}`);
  cell.text(`Toplanti Tarihi: ${moment(toplanti.tarih).format('DD.mm.yyyy')}`);
  cell.text(`Toplantı Yeri  : ${toplanti.yer}`);


  var table = doc.table({
    widths: [1.5 * pdf.cm, 
        null,
        //1.5 * pdf.cm, 
        3 * pdf.cm, 
        3 * pdf.cm, 
        2.5 * pdf.cm],
    borderHorizontalWidths: function (i) {
      return i < 2 ? 1 : 0.1;
    },
    padding: 5,
  });

  var tr = table.header({
    //font: fonts.HelveticaBold,
    font: require("pdfjs/font/Helvetica"),
    borderBottomWidth: 1.5,
  });
  tr.cell("#");
  tr.cell("Karar");
  tr.cell("Takip");
  tr.cell("Son Tarih");
  tr.cell("Durum");

  function addRow(no, karar, atama, sontarih, durum) {
    var tr = table.row();
    tr.cell(no.toString());
    tr.cell(karar);
    tr.cell(atama);
    tr.cell(sontarih);
    tr.cell(durum);
  }


  toplanti.kararlar.map((k,i) => {
    addRow(i+1, k.karar, '', k.sonTarih, '')
  })

  return doc.asBuffer();
};

module.exports = {
  createToplantiPdf,
};
