const jwt = require("jsonwebtoken");
const Personel = require('../models/personel');

//const User = require("../models/user");

let publicUrls = [
    '/v1/auth/signin',
    '/v1/auth/register',
    '/v1/auth/access_token'
]

const TokenControl = async (req, res, next) => {
    for (let i = 0; i < publicUrls.length; ++i) {
        if (publicUrls[i] === req.path) {
            next()
            return
        }
    }
    const token = req.headers.authorization.replace("Bearer ", "").trim();
    //console.log('token: ', token)
    if (token) {
        jwt.verify(token, 'SELAM', (err, data) => {
            if (err) {

            }
            else {
                Personel.findOne({uniqueId : data.uniqueId}).lean().then(p => {
                    if (p) {
                        req.tokenUser = p
                        next()
                    }
                })
                //User.findById(data.id).then(u => {
                //    req.tokenUser = u
                //    next()
                //}).catch(e => next(e))
            }
        });
    }
}

module.exports = {
    TokenControl
}